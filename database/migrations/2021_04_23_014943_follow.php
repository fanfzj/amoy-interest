<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Follow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('follow', function (Blueprint $table) {
            $table->id();
            $table->integer('topic_id')->foreign('topic_id')->nullable()->comment('趣点id')->references('id')->on('topic')->onDelete('cascade');
            $table->integer('user_id')->foreign('user_id')->nullable()->comment('用户id')->references('id')->on('user')->onDelete('cascade');
            $table->timestamp('create_time')->nullable()->comment('创建时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('txq_follow');
    }
}
