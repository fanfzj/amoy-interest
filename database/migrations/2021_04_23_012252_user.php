<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class User extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->id();
            $table->string('account', 16)->nullable()->comment('账号');
            $table->string('password', 32)->nullable()->comment('密码');
            $table->smallInteger('sex')->nullable()->default(1)->comment('性别');
            $table->string('mobile', 32)->comment('手机号');
            $table->string('email', 128)->comment('邮箱');
            $table->string('nickname', 64)->comment('用户昵称');
            $table->string('image')->nullable()->comment('用户头像');
            $table->text('resume')->comment('个人简介');
            $table->timestamp('create_time')->nullable()->comment('创建时间');
            $table->timestamp('update_time')->nullable()->comment('更新时间');
            $table->smallInteger('del')->nullable()->default(0)->comment('逻辑删除');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('txq_user');
    }
}
