<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Topic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topic', function (Blueprint $table) {
            $table->id();
            $table->integer('interest_id')->foreign('interest_id')->nullable()->comment('兴趣id')->references('id')->on('interest')->onDelete('cascade');
            $table->integer('user_id')->foreign('user_id')->nullable()->comment('用户id')->references('id')->on('user')->onDelete('cascade');
            $table->string('name', 32)->nullable()->comment('趣点名');
            $table->integer('subscripe_num')->nullable()->default(0)->comment('订阅数');
            $table->text('discribe')->comment('内容');
            $table->string('image')->nullable()->comment('趣点图像');
            $table->timestamp('create_time')->nullable()->comment('创建时间');
            $table->timestamp('update_time')->nullable()->comment('更新时间');
            $table->smallInteger('del')->nullable()->default(0)->comment('逻辑删除');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('txq_topic');
    }
}
