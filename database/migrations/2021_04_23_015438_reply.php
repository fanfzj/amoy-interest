<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Reply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reply', function (Blueprint $table) {
            $table->id();
            $table->integer('blog_id')->foreign('blog_id')->nullable()->comment('微博id')->references('id')->on('blog')->onDelete('cascade');
            $table->integer('user_id')->foreign('user_id')->nullable()->comment('用户id')->references('id')->on('user')->onDelete('cascade');
            $table->text('content')->comment('内容');
            $table->timestamp('create_time')->nullable()->comment('创建时间');
            $table->timestamp('update_time')->nullable()->comment('更新时间');
            $table->smallInteger('del')->nullable()->default(0)->comment('逻辑删除');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('txq_reply');
    }
}
