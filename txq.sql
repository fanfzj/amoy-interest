﻿# Host: localhost  (Version: 5.7.26)
# Date: 2021-06-03 11:32:01
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "txq_admin"
#

DROP TABLE IF EXISTS `txq_admin`;
CREATE TABLE `txq_admin` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '账号',
  `password` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '密码',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `del` smallint(6) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "txq_admin"
#

/*!40000 ALTER TABLE `txq_admin` DISABLE KEYS */;
INSERT INTO `txq_admin` VALUES (1,'admin','123456','2021-05-08 17:55:04',0);
/*!40000 ALTER TABLE `txq_admin` ENABLE KEYS */;

#
# Structure for table "txq_blog"
#

DROP TABLE IF EXISTS `txq_blog`;
CREATE TABLE `txq_blog` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) DEFAULT NULL COMMENT '趣点id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  `subscripe_num` int(11) DEFAULT '0' COMMENT '评论数',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '微博图像',
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '微博视频',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `del` smallint(6) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "txq_blog"
#

/*!40000 ALTER TABLE `txq_blog` DISABLE KEYS */;
INSERT INTO `txq_blog` VALUES (1,1,1,'早餐打卡',14,'upload/blog_img/Gvbgz9VVu5GDhYloI8XHZ6KdxPU7Ow4qdrZCZyPs.png','upload/blog_video/coffee.mp4','2021-05-07 09:08:02','2021-05-07 09:08:02',0);
/*!40000 ALTER TABLE `txq_blog` ENABLE KEYS */;

#
# Structure for table "txq_follow"
#

DROP TABLE IF EXISTS `txq_follow`;
CREATE TABLE `txq_follow` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) DEFAULT NULL COMMENT '趣点id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "txq_follow"
#

/*!40000 ALTER TABLE `txq_follow` DISABLE KEYS */;
INSERT INTO `txq_follow` VALUES (1,4,1,'2021-05-06 09:32:39'),(3,3,1,'2021-05-07 04:25:44'),(5,1,1,'2021-05-28 09:17:51'),(12,2,1,'2021-05-28 09:27:02');
/*!40000 ALTER TABLE `txq_follow` ENABLE KEYS */;

#
# Structure for table "txq_interest"
#

DROP TABLE IF EXISTS `txq_interest`;
CREATE TABLE `txq_interest` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '兴趣名',
  `discribe` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述',
  `topic_num` int(11) DEFAULT '0' COMMENT '趣点数',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `del` smallint(6) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "txq_interest"
#

/*!40000 ALTER TABLE `txq_interest` DISABLE KEYS */;
INSERT INTO `txq_interest` VALUES (1,'萌宠','无',1,'2021-05-06 07:09:02','2021-05-06 07:09:02',0),(3,'书法','无',0,'2021-05-06 07:09:02','2021-05-06 07:09:02',0),(4,'料理','无',0,'2021-05-06 07:09:02','2021-05-06 07:09:02',0),(5,'电影','无',0,'2021-05-06 07:09:02','2021-05-06 07:09:02',0),(6,'二次元','无',0,'2021-05-06 07:09:02','2021-05-06 07:09:02',0),(7,'aaa','asdada',0,'2021-05-11 05:04:36','2021-05-11 05:24:10',1);
/*!40000 ALTER TABLE `txq_interest` ENABLE KEYS */;

#
# Structure for table "txq_migrations"
#

DROP TABLE IF EXISTS `txq_migrations`;
CREATE TABLE `txq_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "txq_migrations"
#

/*!40000 ALTER TABLE `txq_migrations` DISABLE KEYS */;
INSERT INTO `txq_migrations` VALUES (1,'2021_04_23_012252_user',1),(2,'2021_04_23_013115_topic',1),(3,'2021_04_23_013918_admin',1),(4,'2021_04_23_014031_interest',1),(5,'2021_04_23_014943_follow',1),(6,'2021_04_23_015213_blog',1),(7,'2021_04_23_015438_reply',1);
/*!40000 ALTER TABLE `txq_migrations` ENABLE KEYS */;

#
# Structure for table "txq_reply"
#

DROP TABLE IF EXISTS `txq_reply`;
CREATE TABLE `txq_reply` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) DEFAULT NULL COMMENT '微博id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `content` text COMMENT '内容',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `del` smallint(6) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='评论表';

#
# Data for table "txq_reply"
#

/*!40000 ALTER TABLE `txq_reply` DISABLE KEYS */;
INSERT INTO `txq_reply` VALUES (1,1,1,'ssss','2021-05-28 05:57:19','2021-05-28 05:57:19',0),(2,1,1,'ssss','2021-05-28 06:05:33','2021-05-28 06:05:33',0),(3,1,1,'233','2021-05-28 06:08:27','2021-05-28 06:08:27',0),(4,1,1,'333','2021-05-28 06:08:53','2021-05-28 06:08:53',0),(5,1,1,'1111','2021-05-28 06:10:27','2021-05-28 06:10:27',0),(6,1,1,'333','2021-05-28 06:25:57','2021-05-28 06:25:57',0),(7,1,1,'222','2021-05-28 06:26:56','2021-05-28 06:26:56',0),(8,1,1,'333','2021-05-28 06:27:13','2021-05-28 06:27:13',0),(9,1,1,'1111','2021-05-28 06:27:39','2021-05-28 06:27:39',0),(10,1,1,'444','2021-05-28 06:27:44','2021-05-28 06:27:44',0),(11,1,1,'2','2021-05-28 06:38:19','2021-05-28 06:38:19',0),(12,1,1,'3','2021-05-28 06:38:40','2021-05-28 06:38:40',0),(13,1,1,'4','2021-05-28 06:39:01','2021-05-28 06:39:01',0),(14,1,1,'5','2021-05-28 06:39:04','2021-05-28 06:39:04',0);
/*!40000 ALTER TABLE `txq_reply` ENABLE KEYS */;

#
# Structure for table "txq_topic"
#

DROP TABLE IF EXISTS `txq_topic`;
CREATE TABLE `txq_topic` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `interest_id` int(11) DEFAULT NULL COMMENT '兴趣id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `name` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '趣点名',
  `subscripe_num` int(11) DEFAULT '0' COMMENT '订阅数',
  `discribe` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '趣点图像',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `del` smallint(6) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "txq_topic"
#

/*!40000 ALTER TABLE `txq_topic` DISABLE KEYS */;
INSERT INTO `txq_topic` VALUES (1,1,1,'喵星人的日常',34142,'喵星人的日常','upload/topic_img\\2.jpg','2021-05-06 07:09:02','2021-05-28 09:17:51',0),(2,3,1,'手写你最爱的一句话',12648,'手写你最爱的一句话','upload/topic_img\\1.jpg','2021-05-06 07:09:02','2021-05-28 09:27:02',0),(3,4,1,'365天早餐不重样',86148,'365天早餐不重样','upload/topic_img\\4.jpg','2021-05-06 07:09:02','2021-05-07 04:25:44',0),(4,5,1,'爱你300遍',10172,'爱你300遍','upload/topic_img\\3.jpg','2021-05-06 07:09:02','2021-05-06 09:35:53',0),(5,6,1,'请问要来点兔子吗',6700,'请问要来点兔子吗','upload/topic_img\\5.jpg','2021-05-06 07:09:02','2021-05-06 07:09:02',0);
/*!40000 ALTER TABLE `txq_topic` ENABLE KEYS */;

#
# Structure for table "txq_user"
#

DROP TABLE IF EXISTS `txq_user`;
CREATE TABLE `txq_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '密码',
  `sex` smallint(6) DEFAULT '1' COMMENT '性别',
  `mobile` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '手机号',
  `email` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '邮箱',
  `nickname` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户昵称',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户头像',
  `resume` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '个人简介',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `del` smallint(6) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "txq_user"
#

/*!40000 ALTER TABLE `txq_user` DISABLE KEYS */;
INSERT INTO `txq_user` VALUES (1,'fzj','$2y$10$zgxlcVWBAxXpVyZa4RW3ienGhZ/9n53JAsoAT4/SKCDl4tIE9j.Xm',0,'15900792449','1009137312@qq.com','魔都子沐','upload/user_img/kfYjYSyZIXy0Uekt45R5SYmn3QisCVdJNzsDpiXU.jpg','有一天一定会好的，当你见识过妖魔鬼怪，你再也不会觉得鬼可怕了。','2021-05-06 07:09:02','2021-05-06 07:09:02',0),(4,'fzj1234','$2y$10$hhplBHTcWXnwXfBrPqz9TeLeU/9rv0WiyJHdNcn.V8F8qxyVeQsjW',0,'15900792449','1009137312@qq.com','魔都子沐','upload/user_img/ckdcPcTqh0SQSEHtZjC7zlMdxRx0d6IUrYF619sT.jpg','','2021-06-03 03:09:26','2021-06-03 03:09:26',0),(5,'fzj123433','$2y$10$Ts/dZ6h0LyFNzU5eNd.sl.671tYn.FEeLvLMoC998fH0ulRkx3arS',0,'15900792449','1009137312@qq.com','魔都子沐','upload/user_img/ckdcPcTqh0SQSEHtZjC7zlMdxRx0d6IUrYF619sT.jpg','','2021-06-03 03:23:25','2021-06-03 03:23:25',0);
/*!40000 ALTER TABLE `txq_user` ENABLE KEYS */;
