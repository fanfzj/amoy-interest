<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',"IndexController@index");
Route::get('/login', function () {
  return View('login');
});
Route::get('/admin/login', function () {
    return View('admin.login');
});
Route::get('/register', function () {
    return View('register');
});
Route::post('/dologin', 'UserController@dologin');
Route::post('/doregister', 'UserController@doregister');
Route::get('/logout', 'UserController@logout');
Route::middleware(['login'])->prefix('user')->group(function () {
    //个人中心
    Route::get('/center', 'UserController@center');
    //获取用户信息
    Route::get('/info/{id}','UserController@info');
});
Route::middleware(['login'])->prefix('topic')->group(function () {
    //我的趣点
    Route::get('', 'TopicController@index');
    //创建趣点
    Route::post('/create', 'TopicController@create');
    //订阅
    Route::post('/subscripe',"TopicController@subscripe");
});
Route::prefix('topic')->group(function () {
    //搜索
    Route::get('/search', 'TopicController@search');
    //详细
    Route::get('/{id}',"TopicController@info");
});
Route::middleware(['login'])->prefix('blog')->group(function () {
    //我的微博
    Route::get('', 'BlogController@index');
    //发布评论
    Route::post('/doreply',"BlogController@doreply");
    //发布微博操作
    Route::post('/doadd',"BlogController@doadd");
    //发布微博页面
    Route::get('/add/{id}',"BlogController@add");
});
Route::prefix('blog')->group(function () {
    //微博详细
    Route::get('/{id}',"BlogController@info");
});
Route::prefix('admin')->group(function () {
    //登录操作
    Route::post('/dologin', 'AdminController@dologin');
    //登录页面
    Route::get('/login', function () {
        return View('admin.login');
    });
});
Route::middleware(['admin_login'])->prefix('admin')->group(function () {
    //创建兴趣
    Route::get('/add', function () {
        return View('admin.add');
    });
    Route::post('/doadd', 'AdminController@interest_add');
    //兴趣管理
    Route::get('/index', 'AdminController@interest_index');
    //兴趣删除
    Route::post('/del/{id}', 'AdminController@interest_del');
    //兴趣还原
    Route::post('/reset/{id}', 'AdminController@interest_reset');
    //登出
    Route::get('/logout', 'AdminController@logout');
});
