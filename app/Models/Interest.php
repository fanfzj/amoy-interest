<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Eloquent;
/**
 * Post
 *
 * @mixin Eloquent
 */
class Interest extends Model
{
    protected $table = 'interest';
    protected $fillable = ['name', 'discribe','topic_num'];
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    //一对一，兴趣下所有趣点
    public function topic(){
        return $this->belongsTo('App\Models\Topic');
    }
}
