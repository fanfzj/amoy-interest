<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Eloquent;
/**
 * Post
 *
 * @mixin Eloquent
 */
class Admin extends Model
{
    /**
     * 与模型关联的表名
     *
     * @var string
     */
    protected $table = 'admin';
    /**
     * 指示模型是否自动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
    const CREATED_AT = 'create_time';
}
