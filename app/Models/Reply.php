<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Eloquent;
/**
 * Post
 *
 * @mixin Eloquent
 */
class Reply extends Model
{
    protected $table = 'reply';
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    protected $fillable = ['blog_id', 'user_id','content'];
    //反向关联
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    //反向关联
    public function blog(){
        return $this->belongsTo('App\Models\Blog');
    }
}
