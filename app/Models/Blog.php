<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
/**
 * Post
 *
 * @mixin Eloquent
 */
class Blog extends Model
{
    protected $table = 'blog';
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    protected $fillable = ['topic_id', 'user_id','content', 'subscripe_num','video', 'image'];
    //反向关联
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    //反向关联
    public function topic(){
        return $this->belongsTo('App\Models\Topic');
    }
    //一对多，一个博客所有评论
    public function replies(){
        return $this->hasMany('App\Models\Reply');
    }
}
