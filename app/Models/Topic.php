<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
/**
 * Post
 *
 * @mixin Eloquent
 */
class Topic extends Model
{
    protected $table = 'topic';
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    protected $fillable = ['interest_id', 'user_id','name', 'subscripe_num','discribe', 'image'];
    //多对多，订阅这个趣点所有用户
    public function users(){
        return $this->belongsToMany('App\Models\User', 'follow')->using('App\Models\Follow');
    }
    // 一对多，趣点下所有博客
    public function blogs(){
        return $this->hasMany('App\Models\Blog');
    }
    // 反向一对多，趣点创建者信息
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    // 反向一对多，趣点对应兴趣信息
    public function interest(){
        return $this->belongsTo('App\Models\Interest');
    }
}
