<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Eloquent;
/**
 * Post
 *
 * @mixin Eloquent
 */
class Follow extends Pivot
{
    protected $table = 'follow';
    public $timestamps = false;
    // 反向一对多，订阅获取用户
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    // 反向一对多，订阅获取趣点
    public function blog(){
        return $this->belongsTo('App\Models\Topic');
    }
}
