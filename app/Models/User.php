<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and')
 * @method static Builder create(array $attributes = [])
 */
class User extends Model
{
    protected $table = 'user';
    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = true;
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    protected $fillable = ['account', 'password','sex','mobile', 'email','nickname','image', 'resume'];
    //多对多，用户订阅的所有趣点
    public function topics(){
        return $this->belongsToMany(Topic::class, 'follow')->using(Follow::class);
    }
     //一对多，一个用户所有评论
    public function replies(){
         return $this->hasMany('App\Models\Reply');
     }
    //一对多，一个用户创建所有微博
    public function blogs(){
        return $this->hasMany('App\Models\Blog', 'user_id');
    }
    //一对多，一个用户创建所有趣点
    public function topic(){
        return $this->hasMany('App\Models\Topic', 'user_id');
    }
}
