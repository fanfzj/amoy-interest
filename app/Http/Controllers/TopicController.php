<?php

namespace App\Http\Controllers;

use App\Models\Follow;
use App\Models\Interest;
use App\Models\Topic;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    //我的趣点
    public function index()
    {
        $user_ob = User::find(Session::get('user_id'));
        $interests = Interest::where('del',0)->get();
        return view('topic.index', ['user_ob' => $user_ob, 'interests' => $interests]);
    }
    //创建趣点
    public function create(Request $request)
    {
        $image = $request->file('image')->store('upload/topic_img');
        $interest_id = $request->get('interest_id');
        $name = $request->get('name');
        $discribe = $request->get('discribe', '');
        $rows = Topic::create([
            'interest_id' => $interest_id,
            'name' => $name,
            'discribe' => $discribe,
            'image'=>$image,
            'user_id' => Session::get('user_id')
        ]);
        if ($rows) {
            //更新兴趣的趣点数
            $interest_obj=Interest::find($interest_id);
            $interest_obj->topic_num=$interest_obj->topic_num+1;
            $interest_obj->save();
            //跳转到后台的首页
            $returnJson = [
                'code' => 200
            ];
        } else {
            $returnJson = [
                'code' => 0,
                'message' => '注册信息不完整'
            ];
        }
        return response()->json($returnJson);
    }
    //搜索趣点
    public function search(Request $request){
        $topic_name = $request->get('topic_name');
        $topics=Topic::where('name','like','%'.$topic_name.'%')->get();
        return view('topic.search',['topics'=>$topics]);
    }
    //趣点详情
    public function info(Request $request,$id){
        $topic=Topic::find($id);
        return view('topic.info',['topic'=>$topic]);
    }
    //订阅趣点
    public function subscripe(Request $request){
        $topic_id = $request->get('topic_id');
        $type = $request->get('type');
        $topic_obj=Topic::find($topic_id);
        if($topic_obj) {
            $follow_obj = Follow::where('topic_id', $topic_id)->where('user_id', Session::get('user_id'))->first();
            if(!$follow_obj and $type=='join'){
                $rows=Follow::create([
                    'topic_id' => $topic_id,
                    'user_id' => Session::get('user_id'),
                    'create_time'=>now()
                ]);
                if ($rows) {
                    //更新趣点订阅数
                    $topic_obj->subscripe_num=$topic_obj->subscripe_num+1;
                    $follow_obj=Session::get('follow_list');
                    array_push($follow_obj,$topic_id);
                    $request->session()->put('follow_list',$follow_obj);
                    $topic_obj->save();
                }
            }
            else if($follow_obj and $type=='unjoin') {
                $rows=$follow_obj->delete();
                if($rows){
                    $topic_obj->subscripe_num=$topic_obj->subscripe_num-1;
                    $request->session()->put('follow_list', array_diff(Session::get('follow_list'),[$topic_id]));
                    $topic_obj->save();
                }
            }
            else{
                return response()->json($type=='unjoin'?[
                    'code' => 200,
                    'subscripe_num'=>$topic_obj->subscripe_num
                ]:[
                    'code' => 0,
                    'message' => '已订阅，不可重复订阅'
                ]);
            }
        }
        else{
            return response()->json( [
                'code' => 0,
                'message' => '趣点不存在'
            ]);
        }
    }
}
