<?php

namespace App\Http\Controllers;

use App\Models\Interest;
use App\Models\Topic;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $interests = Interest::where('del',0)->get();
        //根据兴趣查趣点
        $interest_id = $request->get('interest_id',0);
        if($interest_id==0){
            $topics=Topic::where('del',0)->get();
        }
        else{
            $topics = Topic::where('del',0)->where('interest_id',$interest_id)->get();
        }
        return view('index',['topics'=>$topics,'interests'=>$interests,'interest_id'=>$interest_id]);
    }
}