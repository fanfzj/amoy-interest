<?php


namespace App\Http\Controllers;
use App\Models\Interest;
use App\Models\Admin;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //兴趣管理
    public function interest_index(){
        $interests=Interest::all();
        return view('admin.index',['interests'=>$interests]);
    }

    //增加兴趣
    public function interest_add(Request $request){
        $name = $request->get('name');
        $discribe = $request->get('discribe');
        $rows = Interest::create([
            'name' => $name,
            'discribe' => $discribe,
        ]);
        if ($rows) {
            $returnJson = [
                'code' => 200
            ];
        } else {
            $returnJson = [
                'code' => 0,
                'message' => '注册信息不完整'
            ];
        }
        return response()->json($returnJson);
    }
    //登录
    public function dologin(Request $request){
        $account = $request->get('account');
        $password = $request->get('password');
        $ob = Admin::where('account', $account)->first();
        if ($ob) {
            if($password==$ob->password) {
                //创建session
                Session::put(['admin_username' => $account, 'admin_user_id' => $ob->id]);
                //跳转到后台的首页
                $returnJson = [
                    'code' => 200
                ];
            } else {
                $returnJson = [
                    'code' => 0,
                    'message' => '密码不正确'
                ];
            }
        } else {
            $returnJson = [
                'code' => 0,
                'message' => '用户名不存在'
            ];
        }
        return response()->json($returnJson);
    }
    //退出登录
    public function logout()
    {
        Session::flush();
        return redirect('admin/login');
    }
    //兴趣删除
    public function interest_del(Request $request,$id){
        $inserte_obj=Interest::find($id);
        $inserte_obj->del=1;
        $rows=$inserte_obj->save();
        if ($rows) {
            $returnJson = [
                'code' => 200
            ];
        } else {
            $returnJson = [
                'code' => 0,
                'message' => '注册信息不完整'
            ];
        }
        return response()->json($returnJson);
    }
    //兴趣还原
    public function interest_reset(Request $request,$id){
        $inserte_obj=Interest::find($id);
        $inserte_obj->del=0;
        $rows=$inserte_obj->save();
        if ($rows) {
            //跳转到后台的首页
            $returnJson = [
                'code' => 200
            ];
        } else {
            $returnJson = [
                'code' => 0,
                'message' => '注册信息不完整'
            ];
        }
        return response()->json($returnJson);
    }
}
