<?php

namespace App\Http\Controllers;
use App\Models\Topic;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    //登录验证
    function dologin(Request $request)
    {
        $account = $request->get('account');
        $password = $request->get('password');
        $ob = User::where('account', $account)->first();
        if ($ob) {
            if (password_verify($password, $ob->password)) {
                //获取关注列表
                $follow_list=array();
                foreach ($ob->topics as $topic) {
                    array_push($follow_list,$topic->id);
                }
                //创建session
                Session::put(['username' => $account, 'user_id' => $ob->id, 'nickname' => $ob->nickname, 'image' => $ob->image,'follow_list'=>$follow_list]);
                //跳转到后台的首页
                $returnJson = [
                    'code' => 200
                ];
            } else {
                $returnJson = [
                    'code' => 0,
                    'message' => '密码不正确'
                ];
            }
        } else {
            $returnJson = [
                'code' => 0,
                'message' => '用户名不存在'
            ];
        }
        return response()->json($returnJson);
    }

    //注册操作
    public function doregister(Request $request)
    {
        $image = $request->file('image')->store('upload/user_img');
        $account = $request->get('account');
        $password = password_hash($request->get('password'), PASSWORD_BCRYPT);
        $sex = $request->get('sex');
        $email = $request->get('email', '');
        $mobile = $request->get('mobile', '0');
        $resume = $request->get('resume', '');
        $nickname = $request->get('nickname', '');
        $ob = User::where('account', $account)->doesntExist();
        if ($ob!=false) {
            $rows = User::create(['account' => $account,'password' => $password,'sex' => $sex,'email' => $email,'mobile' => $mobile,'resume' => $resume,'nickname' => $nickname,'image' => $image]);
            if ($rows) {
                //跳转到后台的首页
                $returnJson = ['code' => 200];
            } else {
                $returnJson = ['code' => 0,'message' => '注册信息不完整'];
            }
        } else {
            $returnJson = ['code' => 0,'message' => '该账号已存在'];
        }
        return response()->json($returnJson);
    }

    //登出
    public function logout()
    {
        Session::flush();
        return redirect('login');
    }

    //个人中心
    public function center()
    {
        $user_ob = User::find(Session::get('user_id'));
        return view('user.center', ['user_ob' => $user_ob]);
    }
    //用户信息
    public function info(Request $request,$id){
        $user=User::find($id);
        if ($user) {
            //跳转到后台的首页
            $returnJson = [
                'code' => 200,
                'data'=>$user
            ];
        } else {
            $returnJson = [
                'code' => 0,
                'message' => '信息不完整'
            ];
        }
        return response()->json($returnJson);
    }
}
