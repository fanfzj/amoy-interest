<?php

namespace App\Http\Middleware;

use Closure;
use \Illuminate\Support\Facades\Session;
class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if(Session::has('user_id')){
           return $next($request);
       }
       else{
           if($request->getMethod()=='GET'){
               return redirect('login')->with('message','请先登录');
           }
           else{
               return response()->json($returnJson = [
                   'code' => -1
               ]);
           }
       }
    }
}
