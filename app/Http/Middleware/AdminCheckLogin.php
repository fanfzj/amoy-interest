<?php


namespace App\Http\Middleware;

use Closure;
use \Illuminate\Support\Facades\Session;

class AdminCheckLogin
{
    public function handle($request, Closure $next)
    {
        if(Session::has('admin_user_id')){
            return $next($request);
        }
        else{
            if($request->getMethod()=='GET'){
                return redirect('admin/login')->with('message','请先登录');
            }
            else{
                return response()->json($returnJson = [
                    'code' => -1
                ]);
            }
        }
    }
}
