@extends('layouts.base')
@section('title','登录')
@section('body')
    <div class="main">
        <div class="d-flex align-items-center justify-content-around">
            <img src="{{asset('img/logo.png')}}" alt="淘兴趣" class="d-flex align-items-center">
            <h1 class="title text-center">淘兴趣<p>关注你感兴趣的人和事</p></h1>
        </div>
        <form class="form" action="{{ url('user/register') }}" method="POST" id="login_form">
            @csrf
            <div class="form-group p-2">
                <label for="account">账号：</label>
                <input type="text" id="account" name="account" class="form-control" placeholder="请输入账号"
                       required='required'/>
            </div>
            <div class="form-group p-2">
                <label for="password">密码：</label>
                <input type="password" id="password" name="password" class="form-control" placeholder="请输入密码"
                       required='required'/>
            </div>
            <div class="form-group">
                <p id='message' class="text-danger text-center">@isset($message) {{ $message }} @endisset</p>
            </div>
            <div class="form-group p-2 d-flex align-items-center">
                <input id='login_btn' class="mr-auto btn btn-primary" type="button" value="登录">
                <a id='to_register' href="{{ url('register') }}" class="btn btn-link">注册</a>
            </div>
        </form>
    </div>
    <script src="{{asset('js/login.js')}}"></script>
@endsection
