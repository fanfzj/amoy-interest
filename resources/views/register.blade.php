@extends('layouts.base')
@section('title','注册')
@section('body')
<div class="main">
    <div class=" d-flex align-items-center justify-content-around">
        <img src="{{asset('img/logo.png')}}" alt="淘兴趣" class="d-flex align-items-center">
        <h1 class="title text-center align-self-center">淘兴趣<p>关注你感兴趣的人和事</p></h1>
    </div>
    <form class="form p-2" enctype="multipart/form-data" id="register_form">
        @csrf
        <div class="form-group">
            <label for="account" class="two">账号：</label>
            <input type="text" id="account" name="account" class="form-control" placeholder="请输入账号"
                   required='required'/>
        </div>
        <div class="form-group">
            <label for="password">密码：</label>
            <input type="password" id="password" name="password" class="form-control" placeholder="请输入密码"
                   required='required'/>
        </div>
        <div class="form-group">
            <label for="password_again">确认密码：</label>
            <input type="password" id="password_again" name="password_again" class="form-control" placeholder="请再次输入密码"
                   required='required'/>
        </div>
        <div class="form-group">
            <label>性别：</label>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sex" value="0" id="sex_man">
                <label class="form-check-label" for="sex_man">男</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sex" value="1" id="sex_woman">
                <label class="form-check-label" for="sex_woman">女</label>
            </div>
        </div>
        <div class="form-group">
            <label for="email">邮箱：</label>
            <input type="email" id="email" name="email" class="form-control" placeholder="请输入邮箱"/>
        </div>
        <div class="form-group">
            <label for="nickname">昵称：</label>
            <input type="text" id="nickname" name="nickname" class="form-control" placeholder="请输入昵称"
                   required='required'/>
        </div>
        <div class="form-group">
            <label for="mobile">手机：</label>
            <input type="text" id="mobile" name="mobile" class="form-control" placeholder="请输入手机号"/>
        </div>
        <div class="form-group">
            <label for="image">头像图片：</label>
            <input type="file" name="image" class="form-control-file" id="image" accept="image/*">
        </div>
        <div class="form-group">
            <label for="resume">个人简介</label>
            <textarea class="form-control" id="resume" rows="2"></textarea>
        </div>
        <div class="form-group">
            <p id='message' class="text-danger text-center"></p>
        </div>
        <div class="form-group d-flex align-items-center">
            <input id='reg_btn' class="mr-auto btn btn-primary" type="button" value="注册">
            <a id='to_login' href="{{ url('login') }}" class="btn btn-link">登录</a>
        </div>
    </form>
</div>
<script src="{{asset('js/login.js')}}"></script>
@endsection
