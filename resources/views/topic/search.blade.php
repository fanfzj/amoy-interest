@extends('layouts.index')
@section('title','搜索')
@section('content')
    <div class="pt-5 pb-5 text-center w-100 search_topic">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>图片</th>
                    <th>趣点</th>
                    <th>关注人数</th>
                </tr>
            </thead>
            <tbody>
            @forelse($topics as $topic)
                <tr>
                    <th scope="row">{{ $topic->id }}</th>
                    <td><img src="/{{ $topic->image }}" class="img-fluid img-thumbnail rounded mx-auto d-block" alt="{{ $topic->name }}"></td>
                    <td><a href="{{ url('topic/'.$topic->id) }}" class="text-decoration-none text-muted">{{ $topic->name }}</a></td>
                    <td>({{ $topic->subscripe_num }})参与</td>
                </tr>
            @empty
                <tr>
                    <td colspan="5"><p class="text-danger text-center">未查询到数据</p></td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
