@extends('layouts.index')
@section('title','我的趣点')
@section('content')
    <div class="user_info pt-5 pb-5 text-center w-100">
        <div class="user_img m-auto">
            <img src="/{{ $user_ob->image }}" class="img-thumbnail rounded-circle" alt="{{ Session::get('nickname') }}"/>
        </div>
        <div class="lead text-white mt-4">
            {{ Session::get('nickname') }}
        </div>
    </div>
    <div class="card text-left w-100">
        <ul class="nav nav-tabs text-center" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" id="topic-tab" data-toggle="tab" href="#topic" role="tab" aria-controls="topic" aria-selected="false">我的趣点</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="create-topic-tab" data-toggle="tab" href="#create-topic" role="tab" aria-controls="create-topic" aria-selected="true">创建趣点</a>
            </li>

        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="topic" role="tabpanel" aria-labelledby="topic-tab">
                <div class="card m-4">
                    <div class="card-header">
                        <h5 class="card-title">我的趣点</h5>
                    </div>
                    <div class="card-body">
                        @forelse($user_ob->topic as $topic)
                            <div class="col-6 p-2 m-0 col float-left">
                                <a href="{{ url('topic/'.$topic->id) }}" target="_parent" class="text-decoration-none text-muted">
                                    <div class="media">
                                        <img src="/{{ $topic->image }}" alt="{{ $topic->name }}" class="topic_img">
                                        <div class="media-body ml-2">
                                            <h5>{{ $topic->name }}</h5>
                                            <p>{{ $topic->discribe }}</p>
                                            <p>订阅数（{{ $topic->subscripe_num }}）</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @empty
                                <div class="alert alert-danger" role="alert">暂无数据</div>
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="create-topic" role="tabpanel" aria-labelledby="create-topic-tab">
                <div class="card m-4">
                    <div class="card-header">
                        <h5 class="card-title">创建趣点</h5>
                    </div>
                    <div class="card-body">
                        <form class="form" action="{{ url('user/register') }}" method="POST" id="topic_form">
                            @csrf
                            <div class="form-group p-2">
                                <label for="interest_id">兴趣：</label>
                                <select name="interest_id" class="custom-select" id="interest_id">
                                    @foreach($interests as $interest)
                                        <option value="{{ $interest->id }}">{{ $interest->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group p-2">
                                <label for="name">趣点名：</label>
                                <input type="text" id="name" name="name" class="form-control" placeholder="请输入趣点名" required='required'/>
                            </div>
                            <div class="form-group p-2">
                                <label for="discribe">内容：</label>
                                <input type="text" id="discribe" name="discribe" class="form-control" placeholder="请输入内容" required='required'/>
                            </div>
                            <div class="form-group">
                                <label for="image">图片：</label>
                                <input type="file" name="image" class="form-control-file" id="image">
                            </div>
                            <div class="form-group">
                                <p id='message' class="text-warming"></p>
                            </div>
                            <div class="form-group p-2 d-flex align-items-center">
                                <input id='create_topic_btn' class="mr-auto btn btn-primary" type="button" value="提交">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('js/topic.js')}}"></script>
@endsection
