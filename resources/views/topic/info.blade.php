@extends('layouts.index')
@section('title','趣点详情')
@section('content')
    <div class="col-3">
        <div class="card p-2">
            <img class="card-img-top" src="/{{ $topic->image }}" alt="{{ $topic->name }}">
            <div class="card-body">
                <h4>{{ $topic->name }}</h4>
                <p id="join">订阅（{{ $topic->subscripe_num }}）</p>
                <p id="join_btn">
                    @if(Session::get('user_id') and !empty(Session::get('follow_list')))
                        @if(in_array($topic->id,Session::get('follow_list'))) <button class="btn btn-warning follow_topic" data-id="{{ $topic->id }}" data-type="unjoin">取消订阅</button> @else <button class="btn btn-primary follow_topic" data-id="{{ $topic->id }}"  data-type="join">订阅</button> @endif
                        <button id="add_blog_btn" class="btn btn-secondary" data-id="{{ $topic->id }}" data-join="@if(in_array($topic->id,Session::get('follow_list'))) 1 @else 0 @endif">参与</button>
                    @else
                        <button class="btn btn-primary follow_topic" data-id="{{ $topic->id }}"  data-type="join">订阅</button>
                        <button id="add_blog_btn" class="btn btn-secondary" data-id="{{ $topic->id }}" data-join="0">参与</button>
                    @endif
            </div>
        </div>
    </div>
    <div class="col-9">
        @forelse($topic->blogs as $blog)
            <div class="mb-2 card">
                <div class="media card-body">
                    <img class="align-self-start mr-3 rounded-circle" src="/{{ $blog->user->image }}" alt="{{ $blog->user->nickname }}" width="64px" height="64px">
                    <div class="media-body">
                        <h5 class="mb-0">{{ $blog->user->nickname }}</h5>
                        <small class="text-muted">{{ $blog->create_time }}</small>
                        <a href="{{ url('blog/'.$blog->id) }}"><p>{{ $blog->content }}</p></a>
                    </div>
                </div>
            </div>
        @empty
            <div class="alert alert-danger" role="alert">暂无数据</div>
        @endforelse
    </div>
    <script src="{{asset('js/topic.js')}}"></script>
@endsection
