@extends('layouts.admin')
@section('title','新增兴趣')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>新增兴趣</h5>
        </div>
        <div class="card-body">
            <form class="card-body">
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="name">兴趣名</label>
                    </div>
                    <input type="text" id="name" name="name" class="form-control" placeholder="请输入兴趣名" required='required'/>
                </div>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="discribe">描 述</label>
                    </div>
                    <textarea name="discribe" class="form-control" placeholder="请输入兴趣描述" id="discribe"></textarea>
                </div>
                <button class="btn btn-primary" type="button" id="add_btn">发布</button>
                <p id="message" class="text-warning"></p>
            </form>
        </div>
    </div>
    <script>
        $('.nav-bar').removeClass('active');
        $('#interest_add').addClass('active');
    </script>
    <script src="{{asset('js/admin.js')}}"></script>
@endsection
