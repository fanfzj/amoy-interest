@extends('layouts.base')
@section('title','后台登录')
@section('body')
    <div class="main">
        <div class="d-flex align-self-center justify-content-around">
            <img src="{{asset('img/logo.png')}}" alt="淘兴趣" class="d-flex align-items-center">
            <h1 class="title text-center">淘兴趣<br><span>关注你感兴趣的人和事</span></h1>
        </div>
        <form class="form" action="{{ url('user/register') }}" method="POST"  id="login_form">
            @csrf
            <div class="form-group p-2">
                <label for="account">账号：</label>
                <input type="text" id="account" name="account" class="form-control" placeholder="请输入账号"
                       required='required'/>
            </div>
            <div class="form-group p-2">
                <label for="password">密码：</label>
                <input type="password" id="password" name="password" class="form-control" placeholder="请输入密码"
                       required='required'/>
            </div>
            <div class="form-group">
                <p id='message' class="text-danger text-center">@isset($message) {{ $message }} @endisset</p>
            </div>
            <div class="form-group p-2">
                <input id='login_btn' class="mr-auto btn btn-primary" type="button" value="登录">
            </div>
        </form>
    </div>
    <script src="{{asset('js/admin.js')}}"></script>
@endsection
