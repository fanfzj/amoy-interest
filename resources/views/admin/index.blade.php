@extends('layouts.admin')
@section('title','兴趣管理')
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">序号</th>
            <th scope="col">兴趣名</th>
            <th scope="col">描述</th>
            <th scope="col">趣点数</th>
            <th scope="col">操作</th>
        </tr>
        </thead>
        <tbody>
        @forelse($interests as $interest)
        <tr>
            <th scope="row">{{ $interest->id }}</th>
            <td>{{ $interest->name }}</td>
            <td>{{ $interest->discribe }}</td>
            <td>{{ $interest->topic_num }}</td>
            <td>@if($interest->del==0)
                    <span data-id="{{ $interest->id }}" class="del_interest btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i> 删除</span>
                @else
                    <span data-id="{{ $interest->id }}" class="reset_interest btn btn-primary btn-sm"><i class="fas fa-retweet"></i> 还原</span>
                @endif</td>
        </tr>
        @empty
            <tr>
                <td colspan="5"><p class="text-danger text-center"> 暂无数据</p></td>
            </tr>
        @endforelse
        </tbody>
    </table>
    <script>
        $('.nav-bar').removeClass('active');
        $('#interest_index').addClass('active');
    </script>
    <script src="{{asset('js/admin.js')}}"></script>
@endsection
