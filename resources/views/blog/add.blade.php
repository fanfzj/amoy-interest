@extends('layouts.index')
@section('title','发布微博')
@section('content')
    <div class="card">
        <div class="media p-2">
            <img class="w-25 mr-3" src="/{{ $topic->image }}" alt="{{ $topic->name }}">
            <div class="media-body t-t-info">
                <h2>#{{ $topic->name }}</h2>
                <p>{{ $topic->discribe }}</p>
            </div>
        </div>
        <div class="mt-2 p-2">
            <form class="card-body" enctype="multipart/form-data" id="blog_form">
                <input type="hidden" id="user_id" name="user_id" value="{{  Session::get('user_id') }}">
                <input type="hidden" id="topic_id" name="topic_id" value="{{ $topic->id }}">
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="content">内容</label>
                    </div>
                    <textarea name="content" class="form-control" placeholder="" id="content"></textarea>
                </div>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text">图片</span>
                    </div>
                    <input class="form-control" name="image" type="file" accept="image/*" id="image">
                </div>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text">视频</span>
                    </div>
                    <input class="form-control" name="video" type="file" accept="video/*" id="video">
                </div>
                <input class="w-50 btn btn-primary float-right" type="button" value="发布" id="blog_add">
                <p id="message" class="text-warning"></p>
            </form>
        </div>
    </div>
    <script src="{{asset('js/blog.js')}}"></script>
@endsection
