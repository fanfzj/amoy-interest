@extends('layouts.index')
@section('title','我的微博')
@section('content')
    <div class="user_info pt-5 pb-5 text-center w-100">
        <div class="user_img m-auto">
            <img src="/{{ $user_ob->image }}" class="img-thumbnail rounded-circle" alt="{{ Session::get('nickname') }}"/>
        </div>
        <div class="lead text-white mt-4">
            {{ Session::get('nickname') }}
        </div>
    </div>
    <div class="card text-left w-100">
        <div class="card-body">
            <div class="card m-4">
                <div class="card-header">
                    <h5 class="card-title">我的微博</h5>
                </div>
                <div class="card-body blog_list">
                    <table class="table table-striped">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">内容</th>
                            <th scope="col">趣点</th>
                            <th scope="col">图片</th>
                        </tr>
                        </thead>
                        <tbody>
                    @forelse($user_ob->blogs as $blog)
                        <tr>
                            <th scope="row">{{ $blog->id }}</th>
                            <td>{{ $blog->content }}</td>
                            <td>{{ $blog->topic->name }}</td>
                            <td><img src="/{{ $blog->image }}" class="img-fluid img-thumbnail rounded mx-auto d-block" alt="{{ $blog->content }}"></td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5"><p class="text-danger text-center"> 暂无数据</p></td>
                        </tr>
                    @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
