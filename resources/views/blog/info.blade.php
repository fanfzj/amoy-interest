@extends('layouts.index')
@section('title','微博详情')
@section('content')
    <div class="card">
        <div class="media card-body">
            <img class="align-self-start mr-3 rounded-circle" src="/{{ $blog->user->image }}" alt="{{ $blog->user->nickname }}" width="64px" height="64px">
            <div class="media-body">
                <h5 class="mb-0">{{ $blog->user->nickname }}</h5>
                <small class="text-muted">{{ $blog->create_time }}</small>
                <p>{{ $blog->content }}
                    @if($blog->image)
                        <img class="card-img" src="/{{ $blog->image }}">
                    @endif
                    @if($blog->video)
                        <video class="card-img" src="/{{ $blog->video }}" controls="controls"></video>
                    @endif
                </p>
            </div>
        </div>
    </div>
    <div class="card" style="width: 100%">
        <div class="card-header">
            评论
        </div>
        <div class="reply card-body">
            <div id="reply_list">
                @forelse($blog->replies as $reply_item)
                    <div class='mb-2 card'>
                        <div class='media card-body'>
                            <img class='align-self-start mr-3 rounded-circle' src='/{{ $reply_item->user->image }}' alt='{{ $reply_item->user->nickname }}' width='64px' height='64px'>
                            <div class='media-body'>
                                <h5 class='mb-0'>{{ $reply_item->user->nickname }}</h5>
                                <small class='text-muted'>{{ $reply_item->create_time }}</small>
                                <p>{{ $reply_item->content }}</p>
                            </div>
                        </div>
                    </div>
                @empty
                    <p class="text-warning">暂无评论，快来留言吧~</p>
                @endforelse
            </div>
            <form id="form_reply">
                <div class="form-group">
                    <label for="reply">发表评论</label>
                    <textarea class="form-control" id="reply" rows="3"></textarea>
                    <small id="message" class="form-text text-muted text-warning"></small>
                </div>
                <input type="hidden" id="blog_id" name="blog_id" value="{{ $blog->id }}">
                <input type="hidden" id="user_id" name="user_id" value="{{ Session::get('user_id') }}">
                <input type="hidden" id="user_nickname" name="user_nickname" value="{{ Session::get('nickname') }}">
                <input type="hidden" id="user_image" name="user_image" value="{{ Session::get('image') }}">
                <button type="button" class="btn btn-primary" id="blog_reply">提交</button>
            </form>
        </div>
    </div>
    <script src="{{asset('js/blog.js')}}"></script>
@endsection
