<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/"><img src="{{asset('img/logo.png')}}" alt="淘兴趣" class="logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <form class="form-inline" action="{{ url('topic/search') }}">
      <input class="form-control mr-sm-2" type="search" placeholder="搜索" aria-label="搜索" name="topic_name">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
    </form>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">首页  <span class="sr-only">(current)</span></a>
      </li>
      @if (Session::get('nickname','')=='')
      <li class="nav-item">
        <a class="nav-link" href="{{ url('login') }}">登录</a>
      </li>
      @else
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ Session::get('nickname') }}
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a href="{{ url('user/center') }}" class="dropdown-item">个人中心</a>
          <a href="{{ url('topic') }}" class="dropdown-item">我的趣点</a>
          <a href="{{ url('blog') }}" class="dropdown-item">我的微博</a>
          <a href="{{ url('logout') }}" class="dropdown-item">登出</a>
        </div>
      </li>
      @endif
    </ul>
  </div>
</nav>
