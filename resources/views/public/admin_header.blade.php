<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap pl-3 pr-3 shadow">
    <a class="navbar-brand" href="/"><img src="{{asset('img/logo.png')}}" alt="淘兴趣" class="logo">淘兴趣</a>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
            <a class="btn btn-primary" href="{{ url('admin/logout') }}">退出登录</a>
        </li>
    </ul>
</nav>
