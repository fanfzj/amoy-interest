@extends('layouts.base')
@section('body')
    @include('public/header')
    <div class="container mt-5 mb-5">
        <div class="row">
            @yield('content')
        </div>
    </div>
    @include("public.footer")
@endsection
