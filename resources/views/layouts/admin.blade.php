@extends('layouts.base')
@section('link')
    <link href="{{asset('fontawesome/css/all.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('body')
    @include('public/admin_header')
    <div class="container-fluid">
        <div class="row">
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="sidebar-sticky pt-3">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link nav-bar" href="{{ url('admin/index') }}" id="interest_index"><i class="fas fa-home"></i> 兴趣管理</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-bar" href="{{ url('admin/add') }}" id="interest_add"><i class="fas fa-plus"></i> 新增兴趣</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4 mt-4" role="main">
            @yield('content')
            </main>
        </div>
    </div>
@endsection
