<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>淘兴趣-@yield('title')</title>
    <link href="{{asset('bootstrap/css/bootstrap.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/public.css')}}" rel="stylesheet" type="text/css"/>
    @yield('link')
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="{{asset('bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
</head>
<body>
@yield('body')
</body>
</html>
