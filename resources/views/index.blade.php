@extends('layouts.index')
@section('title','首页')
@section('content')
<div class="col-md-2 d-md-block d-none">
    <nav class="nav nav-pills flex-column">
        <a class="nav-link @if($interest_id==0) active @endif" href="?">推荐</a>
        @foreach ($interests as $interest)
            <a class="nav-link @if($interest->id==$interest_id) active @endif" href="?interest_id={{ $interest->id }}">{{ $interest->name }}</a>
        @endforeach
    </nav>
</div>
<div class="col-12 d-block d-md-none mb-2">
    <nav class="nav nav-pills d-flex justify-content-between">
        <a class="nav-link @if($interest_id==0) active @endif" href="?">推荐</a>
        @foreach ($interests as $interest)
            <a class="nav-link @if($interest->id==$interest_id) active @endif" href="?interest_id={{ $interest->id }}">{{ $interest->name }}</a>
        @endforeach
    </nav>
</div>
<div class="col-md-10">
    <div class="card-columns">
        @forelse ($topics as $topic)
        <div class="card">
            <a href="{{ url('topic/'.$topic->id) }}" target="_parent" class="text-decoration-none text-muted">
                <div class="card-body">
                    <h5 class="card-title">{{ $topic->name }}</h5>
                    <p class="card-text">{{ $topic->subscripe_num }}人参与</p>
                </div>
                <img src="/{{ $topic->image }}" class="card-img-bottom" alt="{{ $topic->name }}">
            </a>
        </div>
        @empty
            <p class="text-danger text-center"> 暂无数据</p>
        @endforelse
    </div>
</div>
@endsection
