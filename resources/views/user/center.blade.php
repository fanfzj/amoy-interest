@extends('layouts.index')
@section('title','个人中心')
@section('content')
    <div class="user_info pt-5 pb-5 text-center w-100">
        <div class="user_img m-auto">
            <img src="/{{ $user_ob->image }}" class="img-thumbnail rounded-circle" alt="{{ Session::get('nickname') }}"/>
        </div>
        <div class="lead text-white mt-4">
            {{ Session::get('nickname') }}
        </div>
    </div>
    <div class="card text-left w-100">
        <ul class="nav nav-tabs text-center" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" id="center-tab" data-toggle="tab" href="#center" role="tab" aria-controls="home" aria-selected="true">个人主页</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="topic-tab" data-toggle="tab" href="#topic" role="tab" aria-controls="topic" aria-selected="false">关注趣点</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="center" role="tabpanel" aria-labelledby="center-tab">
                <div class="card m-4">
                    <div class="card-header">
                        <h5 class="card-title">个人资料</h5>
                    </div>
                    <div class="card-body">
                        <p>账号：{{ $user_ob->account }}</p>
                        <p>性别：@if($user_ob->sex==0)男@else女@endif</p>
                        <p>手机号：{{ $user_ob->mobile }}</p>
                        <p>邮箱：{{ $user_ob->email }}</p>
                        <p>昵称：{{ $user_ob->nickname }}</p>
                        <p>个人简介：{{ $user_ob->resume }}</p>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="topic" role="tabpanel" aria-labelledby="topic-tab">
                <div class="card m-4">
                    <div class="card-header">
                        <h5 class="card-title">关注的趣点列表</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">趣点名</th>
                                    <th scope="col">兴趣</th>
                                    <th scope="col">内容</th>
                                    <th scope="col">订阅数</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($user_ob->topics as $topic)
                                <tr>
                                    <th scope="row">{{ $topic->id }}</th>
                                    <td>{{ $topic->name }}</td>
                                    <td>{{ $topic->interest->name }}</td>
                                    <td>{{ $topic->discribe }}</td>
                                    <td>{{ $topic->subscripe_num }}</td>
                                </tr>
                                @empty
                                    <tr>
                                        <td colspan="5"><p class="text-danger text-center">暂无数据</p></td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
