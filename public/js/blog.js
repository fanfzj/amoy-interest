$('#blog_add').click(function (){
    $('#message').val('');
    $.ajax({
        type: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/blog/doadd',
        processData: false,
        async: false,
        contentType:false,
        data: new FormData($("#blog_form")[0]),
        dataType: "json",
        success: function (result) {
            if (result.code == 200) {
                window.location.href = "/blog";
            }
            else if(result.code==-1){
                window.location.href='/login';
            }
            else {
                $('#message').text(result.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#message').text(jqXHR.reponseText);
        }
    });
    return false;
})
$("#blog_reply").click(function (){
    if($('#user_id').val()){
        $('#message').text("");
        $.ajax({
            url:'/blog/doreply',
            data:new FormData($('#form_reply')[0]),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'post',
            dataType: "json",
            success: function (result) {
                if (result.code == 200) {
                    var date=new Date();
                    var create_time=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()+" "+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();
                    $('#reply_list').append("<div class='mb-2 card'><div class='media card-body'><img class='align-self-start mr-3 rounded-circle' src='/" + $("#user_image").val() + "' alt='" + $("#user_nickname").val() + "' width='64px' height='64px'><div class='media-body'><h5 class='mb-0'>" + $("#user_nickname").val() + "</h5><small class='text-muted'>" + create_time + "</small><p>" + $('#reply').val() + "</p></span></div></div></div>");
                }
                else if(result.code==-1){
                    window.location.href='/login';
                }
                else {
                    $('#message').text(result.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#message').text(jqXHR.reponseText);
            }
        })
    }
    else{
        alert('请先登录');
        window.location.href='/login';
    }
});
