$('#create_topic_btn').click(function(){
    $('#message').val('');
    $.ajax({
        type: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/topic/create',
        processData: false,
        async: false,
        contentType:false,
        data: new FormData($("#topic_form")[0]),
        dataType: "json",
        success: function (result) {
            if (result.code == 200) {
                window.location.href = "/topic";
            }
            else if(result.code==-1){
                window.location.href='/login';
            }
            else {
                $('#message').text(result.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#message').text(jqXHR.reponseText);
        }
    });
    return false;
});
$('#add_blog_btn').click(function (){
    var topic_id=$(this).data('id');
    var tf_join=$(this).data('join');
    if(tf_join==1){
        window.location.href='/blog/add/'+topic_id;
    }
    else{
        alert('暂未订阅该趣点，不可参与该趣点的微博发布，请先订阅该趣点')
    }
})
$('.follow_topic').click(function (){
    $.ajax({
        type: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/topic/subscripe',
        data: {"topic_id":$(this).data('id'),'type':$(this).data('type')},
        dataType: "json",
        success: function (result) {
            if (result.code == 200) {
                $('#join').html('订阅（'+result.subscripe_num+'）');
                if($("#join_btn button").data('type')=='join'){
                    alert('订阅成功');
                    $(".follow_topic").data('type','unjoin').removeClass('btn-primary').addClass('btn-warning').text('取消订阅');
                    $('#add_blog_btn').data('join',1);
                }
                else{
                    $(".follow_topic").removeClass('btn-warning').addClass('btn-primary').text('订阅').data('type','join');
                    alert('取消订阅成功');
                    $('#add_blog_btn').data('join',0);
                }
            }
            else if(result.code==-1){
                window.location.href='/login';
            }
            else {
                alert(result.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.reponseText);
        }
    });
})
