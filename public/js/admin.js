$('#login_btn').click(function(){
    $('#message').val('');
    $.ajax({
        type: 'post',
        url: '/admin/dologin',
        data: $('#login_form').serialize(),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (result) {
            if (result.code == 200) {
                window.location.href = "/admin/index";
            } else {
                $('#message').text(result.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#message').text(jqXHR.reponseText);
        }
    });
    return false;
});

$('#add_btn').click(function (){
    $.ajax({
        type: 'post',
        url: '/admin/doadd',
        data: {
            'name':$('#name').val(),
            'discribe':$('#discribe').val(),
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (result) {
            if (result.code == 200) {
                window.location.href = "/admin/index";
            } else {
                $('#message').text(result.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#message').text(jqXHR.reponseText);
        }
    });
})
$('.del_interest').click(function(){
    if(confirm('确定要删除吗')==true) {
        $.ajax({
            type: 'post',
            url: '/admin/del/' + $(this).data('id'),
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content')
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            success: function (result) {
                if (result.code == 200) {
                    window.location.href = "/admin/index";
                } else {
                    $('#message').text(result.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#message').text(jqXHR.reponseText);
            }
        });
    }
});
$('.reset_interest').click(function(){
    $.ajax({
        type: 'post',
        url: '/admin/reset/' + $(this).data('id'),
        data: {
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (result) {
            if (result.code == 200) {
                window.location.href = "/admin/index";
            } else {
                $('#message').text(result.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#message').text(jqXHR.reponseText);
        }
    });
});
