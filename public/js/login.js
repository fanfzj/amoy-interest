$('#login_btn').click(function(){
    $('#message').val('');
    $.ajax({
        type: 'post',
        url: '/dologin',
        data: $('#login_form').serialize(),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        success: function (result) {
            if (result.code == 200) {
                window.location.href = "/user/center";
            } else {
                $('#message').text(result.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#message').text(jqXHR.reponseText);
        }
    });
    return false;
});
$('#reg_btn').click(function(){
    $('#message').val('');
    var pwd = $('#password').val(), pwd_again = $('#password_again').val();
    if (pwd == pwd_again) {
        $.ajax({
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '/doregister',
            processData: false,
            async: false,
            contentType:false,
            data: new FormData($("#register_form")[0]),
            dataType: "json",
            success: function (result) {
                if (result.code == 200) {
                    window.location.href = "/login";
                } else {
                    $('#message').text(result.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#message').text(jqXHR.reponseText);
            }
        });
    }
    else{
        $('#message').text('输入的两个密码不一致，请重新输入');
    }
});
